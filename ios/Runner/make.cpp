#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include "detector.hpp"
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing.h>
//#include <dlib/opencv/cv_image.h>
#include <iostream>
#include <dlib/image_io.h>
#include"getShapeFile.hpp"

using namespace std;
//using namespace cv;

static ArucoDetector *detector = nullptr;

void rotateMat(cv::Mat &matImage, int rotation)
{
    if (rotation == 90)
    {
        transpose(matImage, matImage);
        flip(matImage, matImage, 1); // transpose+flip(1)=CW
    }
    else if (rotation == 270)
    {
        transpose(matImage, matImage);
        flip(matImage, matImage, 0); // transpose+flip(0)=CCW
    }
    else if (rotation == 180)
    {
        flip(matImage, matImage, -1); // flip(-1)=180
    }
}

extern "C"
{

//__attribute__((visibility("default"))) __attribute__((used))

__attribute__((visibility("default"))) __attribute__((used))
dlib::frontal_face_detector*
initDetector()

{
    
    dlib::frontal_face_detector*  detectorPntr = new dlib::frontal_face_detector(dlib::get_frontal_face_detector());
    
    
    
    return detectorPntr;
}

__attribute__((visibility("default"))) __attribute__((used))
dlib::shape_predictor*
initShapePredictor()
{
    dlib::shape_predictor* predictor = new dlib::shape_predictor();
    dlib::deserialize(pathPredictor()) >> *predictor;
    return predictor;
}


__attribute__((visibility("default"))) __attribute__((used))
int detectPoint(dlib::shape_predictor* predict, dlib::frontal_face_detector* detector, uint8_t* buffer, int width, int height) {
//    cv::Mat frame;
//    frame = cv::Mat(height, width, CV_8UC4, buffer);
////    cv::cvtColor(frame, frame, cv::COLOR_BGRA2GRAY);
//    cv::resize(frame, frame, cv::Size(), 1.0 / 2, 1.0 / 2);
    dlib::array2d<dlib::bgr_pixel> dlibFrame;
    dlibFrame.set_size(height, width);
    dlibFrame.reset();
    long position = 0;
    while (dlibFrame.move_next()) {
        dlib::bgr_pixel& pixel = dlibFrame.element();

        // assuming bgra format here
        long bufferLocation = position * 4; //(row * width + column) * 4;
        char b = buffer[bufferLocation];
        char g = buffer[bufferLocation + 1];
        char r = buffer[bufferLocation + 2];
        //        we do not need this
        //        char a = baseBuffer[bufferLocation + 3];
        
        dlib::bgr_pixel newpixel(b, g, r);
        pixel = newpixel;
        
        position++;
    }
//    dlib::array2d<dlib::rgb_pixel> dlibFrame(frame.rows, frame.cols);
//    for (int r = 0; r < frame.rows; r++) {
//        for (int c = 0; c < frame.cols; c++) {
//            cv::Vec3b bgrPixel = frame.at<cv::Vec3b>(r, c);
//            dlibFrame[r][c] = dlib::rgb_pixel(bgrPixel[2], bgrPixel[1], bgrPixel[0]);
//        }
//    }
    
    std::vector<dlib::rectangle> faces=(
                                        *detector
                                        )(dlibFrame, 0);
//    printf(faces.size());
    
    return  faces.size();
    
}


__attribute__((visibility("default"))) __attribute__((used))
void killDetector(dlib::frontal_face_detector* detectorPnt)
{
    delete  detectorPnt;
    return;
}

__attribute__((visibility("default"))) __attribute__((used))
void killPredictor(dlib::shape_predictor* predictorPnt)
{
    delete  predictorPnt;
    return;
}



__attribute__((visibility("default"))) __attribute__((used)) void destroyDetector()
{
    if (detector != nullptr)
    {
        delete detector;
        detector = nullptr;
    }
}

__attribute__((visibility("default"))) __attribute__((used))
const float *
detect(int width, int height, int rotation, uint8_t *bytes, bool isYUV, int32_t *outCount)
{
    if (detector == nullptr)
    {
        float *jres = new float[1];
        jres[0] = 0;
        return jres;
    }
    
    cv::Mat frame;
    if (isYUV)
    {
        cv::Mat myyuv(height + height / 2, width, CV_8UC1, bytes);
        cv::cvtColor(myyuv, frame, cv::COLOR_YUV2BGRA_NV21);
    }
    else
    {
        frame = cv::Mat(height, width, CV_8UC4, bytes);
    }
    
    rotateMat(frame, rotation);
    cv::cvtColor(frame, frame, cv::COLOR_BGRA2GRAY);
    vector<ArucoResult> res = detector->detectArucos(frame, 0);
    
    vector<float> output;
    
    for (int i = 0; i < res.size(); ++i)
    {
        ArucoResult ar = res[i];
        // each aruco has 4 corners
        output.push_back(ar.corners[0].x);
        output.push_back(ar.corners[0].y);
        output.push_back(ar.corners[1].x);
        output.push_back(ar.corners[1].y);
        output.push_back(ar.corners[2].x);
        output.push_back(ar.corners[2].y);
        output.push_back(ar.corners[3].x);
        output.push_back(ar.corners[3].y);
    }
    
    // Copy result bytes as output vec will get freed
    unsigned int total = sizeof(float) * output.size();
    float *jres = (float *)malloc(total);
    memcpy(jres, output.data(), total);
    
    *outCount = output.size();
    return jres;
}
}
