#include <iostream>
#include <string>


#include <CoreFoundation/CoreFoundation.h> // for NSBundle




std::string pathPredictor() {
    CFBundleRef mainBundle = CFBundleGetMainBundle();

    // Get a reference to the file's URL
    CFURLRef imageURL = CFBundleCopyResourceURL(mainBundle, CFSTR("shape_predictor_68_face_landmarks"), CFSTR("dat"), NULL);
    CFStringRef imagePath = CFURLCopyFileSystemPath(imageURL, kCFURLPOSIXPathStyle);

    // Get the system encoding method
    CFStringEncoding encodingMethod = CFStringGetSystemEncoding();

    // Convert the string reference into a C string
    const char *path = CFStringGetCStringPtr(imagePath, encodingMethod);
    return path;
    
}
