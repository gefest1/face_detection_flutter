//
//  detector.hpp
//  Runner
//
//  Created by Ulan Batyrbekov on 4/3/23.
//

#pragma once
#include <opencv2/core.hpp>

using namespace std;

struct ArucoResult {
    vector<cv::Point2f> corners;
    int index = -1;
};

struct ArucoDict {
    vector<vector<int>> sigs;
    vector<vector<cv::Point3f>> worldLoc;
};

class ArucoDetector {
public:
    ArucoDetector(cv::Mat marker, int bits);
    vector<ArucoResult> detectArucos(cv::Mat frame, int misses = 0);
    ArucoDict m_dict;

private:
    vector<int> getContoursBits(cv::Mat image, vector<cv::Point2f> cnt, int bits);
    bool equalSig(vector<int>& sig1, vector<int>& sig2, int allowedMisses = 0);
    void orderContour(vector<cv::Point2f>& cnt);
    vector<vector<cv::Point2f>> findSquares(cv::Mat img);
    ArucoDict loadMarkerDictionary(cv::Mat marker, int bits);
    cv::TermCriteria TERM_CRIT = cv::TermCriteria(cv::TermCriteria::EPS + cv::TermCriteria::MAX_ITER, 40, 0.001);
};
