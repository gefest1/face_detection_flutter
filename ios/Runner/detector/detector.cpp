//
//  detector.cpp
//  Runner
//
//  Created by Ulan Batyrbekov on 4/3/23.
//

#include "detector.hpp"
#include <opencv2/imgproc.hpp>


using namespace std;


ArucoDetector::ArucoDetector(cv::Mat marker, int bits) {
    m_dict = loadMarkerDictionary(marker, bits);
}

vector<int> ArucoDetector::getContoursBits(cv::Mat image, vector<cv::Point2f> cnt, int bits) {
    assert(bits > 0 && round(sqrt(bits)) == sqrt(bits));
    int pixelLen = sqrt(bits);

    // NOTE: we assume contour points are clockwise starting from top-left
    vector<cv::Point2f> corners = { cv::Point2f(0, 0), cv::Point2f(bits, 0), cv::Point2f(bits, bits), cv::Point2f(0, bits) };
    cv::Mat M = getPerspectiveTransform(cnt, corners);
    cv::Mat warpped;
    warpPerspective(image, warpped, M, cv::Size(bits, bits));

    cv::Mat binary;
    threshold(warpped, binary, 0, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);

    cv::Mat element = getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));
    erode(binary, binary, element);

    vector<int> res;
    for (int r = 0; r < pixelLen; ++r) {
        for (int c = 0; c < pixelLen; ++c) {
            int y = r * pixelLen + (pixelLen / 2);
            int x = c * pixelLen + (pixelLen / 2);
            if (binary.at<uchar>(y, x) >= 128)
            {
                res.push_back(1);
            }
            else
            {
                res.push_back(0);
            }
        }
    }

    return res;
}

bool ArucoDetector::equalSig(vector<int>& sig1, vector<int>& sig2, int allowedMisses)
{
    int misses = 0;
    for (int i = 0; i < sig1.size(); ++i) {
        if (sig1[i] != sig2[i])
            ++misses;
    }

    return misses <= allowedMisses;
}

void ArucoDetector::orderContour(vector<cv::Point2f>& cnt)
{
    float cx = (cnt[0].x + cnt[1].x + cnt[2].x + cnt[3].x) / 4.0f;
    float cy = (cnt[0].y + cnt[1].y + cnt[2].y + cnt[3].y) / 4.0f;

    // IMPORTANT! We assume the contour points are counter-clockwise (as we use EXTERNAL contours in findContours)
    if (cnt[0].x <= cx && cnt[0].y <= cy)
    {
        swap(cnt[1], cnt[3]);
    }
    else
    {
        swap(cnt[0], cnt[1]);
        swap(cnt[2], cnt[3]);
    }
}

vector<vector<cv::Point2f>> ArucoDetector::findSquares(cv::Mat img) {
    vector<vector<cv::Point2f>> cand;

    cv::Mat thresh;
    adaptiveThreshold(img, thresh, 255, cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY, 11, 5);

    thresh = ~thresh;
    vector<vector<cv::Point>> cnts;
    findContours(thresh, cnts, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

    vector<cv::Point2f> cnt;
    for (int i = 0; i < cnts.size(); ++i)
    {
        approxPolyDP(cnts[i], cnt, 0.05 * arcLength(cnts[i], true), true);
        if (cnt.size() != 4 || contourArea(cnt) < 200 || !isContourConvex(cnt)) {
            continue;
        }

        cornerSubPix(img, cnt, cv::Size(5, 5), cv::Size(-1, -1), TERM_CRIT);

        orderContour(cnt);
        cand.push_back(cnt);
    }

    return cand;
}

vector<ArucoResult> ArucoDetector::detectArucos(cv::Mat frame, int misses) {
    vector<ArucoResult> res;
    vector<vector<cv::Point2f>> cands = findSquares(frame);

    for (int i = 0; i < cands.size() && res.size() < 3; ++i) {
        vector<cv::Point2f> cnt = cands[i];
        vector<int> sig = getContoursBits(frame, cnt, 36);
        for (int j = 0; j < m_dict.sigs.size(); ++j) {
            if (equalSig(sig, m_dict.sigs[j], misses)) {
                ArucoResult ar;
                ar.corners = cnt;
                ar.index = j;
                res.push_back(ar);
                break;
            }
        }
    }

    return res;
}

ArucoDict ArucoDetector::loadMarkerDictionary(cv::Mat marker, int bits) {
    ArucoDict res;
    int w = marker.cols;
    int h = marker.rows;

    cv::cvtColor(marker, marker, cv::COLOR_BGRA2GRAY);

    vector<cv::Point2f> cnt = { cv::Point2f(0, 0), cv::Point2f(w, 0) , cv::Point2f(w, h) , cv::Point2f(0, h) };
    vector<cv::Point3f> world = { cv::Point3f(0, 0, 0), cv::Point3f(25, 0, 0), cv::Point3f(25, 25, 0), cv::Point3f(0, 25, 0) };

    for (int i = 0; i < 4; ++i) {
        vector<int> sig = getContoursBits(marker, cnt, bits);
        res.sigs.push_back(sig);

        vector<cv::Point3f> w(world);
        res.worldLoc.push_back(w);

        rotate(marker, marker, cv::ROTATE_90_CLOCKWISE);

        world.insert(world.begin(), world[3]);
        world.pop_back();
    }

    return res;
}
