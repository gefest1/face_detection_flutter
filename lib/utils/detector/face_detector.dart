import 'dart:developer';
import 'dart:ffi' as ffi;
import 'dart:io';
import 'dart:typed_data';

import 'package:camera/camera.dart';
import 'package:ffi/ffi.dart';

// typedef init_detector_func = Pointer<dynamic> Function();

typedef InitDetectorFunc = ffi.Pointer Function();

final ffi.DynamicLibrary nativeLib = Platform.isAndroid
    ? ffi.DynamicLibrary.open("libnative_opencv.so")
    : ffi.DynamicLibrary.process();
final InitDetectorFunc initDetector = nativeLib
    .lookup<ffi.NativeFunction<InitDetectorFunc>>('initDetector')
    .asFunction();
final killDetector = nativeLib
    .lookup<ffi.NativeFunction<ffi.Void Function(ffi.Pointer)>>("killDetector")
    .asFunction<void Function(ffi.Pointer)>();

final InitDetectorFunc initShape = nativeLib
    .lookup<ffi.NativeFunction<InitDetectorFunc>>('initShapePredictor')
    .asFunction();
final killPredictor = nativeLib
    .lookup<ffi.NativeFunction<ffi.Void Function(ffi.Pointer)>>("killPredictor")
    .asFunction<void Function(ffi.Pointer)>();

typedef DetectPointFunc = ffi.Int32 Function(
  ffi.Pointer predict,
  ffi.Pointer detector,
  ffi.Pointer<ffi.Uint8> buffer,
  ffi.Int32 width,
  ffi.Int32 height,
);
typedef DetectPoint = int Function(
  ffi.Pointer predict,
  ffi.Pointer detector,
  ffi.Pointer<ffi.Uint8> buffer,
  int width,
  int height,
);

final detectFaces =
    nativeLib.lookupFunction<DetectPointFunc, DetectPoint>('detectPoint');

class Detector {
  late ffi.Pointer detectorPtr;
  late ffi.Pointer shapePntr;

  Future<void> init() async {
    detectorPtr = initDetector();
    shapePntr = initShape();
  }

  int detectFace(CameraImage cameraImage) {
    Plane buffer = cameraImage.planes.first;

    // Get a pointer to the first element of the list.

    final ponterList = malloc.allocate<ffi.Uint8>(buffer.bytes.lengthInBytes);
    final bytes = ponterList.asTypedList(buffer.bytes.lengthInBytes);
    bytes.setAll(0, bytes);
    log('FUNCTION STARTED');
    final facesCount = detectFaces(
      shapePntr,
      detectorPtr,
      ponterList,
      cameraImage.width,
      cameraImage.height,
    );
    malloc.free(ponterList);
    return facesCount;
  }

  void dispose() {
    malloc.free(detectorPtr);
    malloc.free(shapePntr);
  }
}
