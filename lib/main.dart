import 'dart:developer';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:clinic/utils/detector/aruco_detector_async.dart';
import 'package:clinic/utils/detector/face_detector.dart';
import 'package:clinic/utils/ffi.dart';
import 'package:flutter/material.dart';
import 'package:collection/collection.dart';
import 'package:flutter/services.dart';

void main() {
  print('pid: $pid');
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with WidgetsBindingObserver {
  int _camFrameRotation = 0;
  CameraController? _camController;
  bool _detectionInProgress = false;
  int _lastRun = 0;
  // late ArucoDetectorAsync _arucoDetector;
  double _camFrameToScreenScale = 0;

  Detector? bigDetector;

  void initDet() async {
    final dd = Detector();
    await dd.init();
    bigDetector = dd;
    log("FINISH DETECTOR INIT");
  }

  @override
  void initState() {
    initCamera();
    initDet();
    // _arucoDetector = ArucoDetectorAsync();

    super.initState();
  }

  @override
  void dispose() {
    _camController!.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    final CameraController? cameraController = _camController;

    // App state changed before we got the chance to initialize.
    if (cameraController == null || !cameraController.value.isInitialized) {
      return;
    }

    if (state == AppLifecycleState.inactive) {
      cameraController.dispose();
    } else if (state == AppLifecycleState.resumed) {
      initCamera();
    }
  }

  Future<void> initCamera() async {
    final cameras = await availableCameras();
    CameraDescription desc = cameras
        .firstWhereOrNull((c) => c.lensDirection == CameraLensDirection.front)!;

    _camFrameRotation = Platform.isAndroid ? desc.sensorOrientation : 0;

    _camController = CameraController(
      desc,
      ResolutionPreset.high, // 720p
      enableAudio: false,

      // imageFormatGroup: Platform.isAndroid
      //     ? ImageFormatGroup.yuv420
      //     : ImageFormatGroup.bgra8888,
    )..lockCaptureOrientation(DeviceOrientation.portraitUp);

    try {
      await _camController!.initialize();
      await _camController!
          .startImageStream((image) => _processCameraImage(image));
    } catch (e) {
      log("Error initializing camera, error: ${e.toString()}");
    }

    if (mounted) {
      setState(() {});
    }
  }

  void _processCameraImage(CameraImage image, [bool active = false]) async {
    // if (!active) return;
    if (_detectionInProgress ||
        !mounted ||
        DateTime.now().millisecondsSinceEpoch - _lastRun < 30) {
      return;
    }
    if (_camFrameToScreenScale == 0) {
      var w = (_camFrameRotation == 0 || _camFrameRotation == 180)
          ? image.width
          : image.height;
      _camFrameToScreenScale = MediaQuery.of(context).size.width / w;
    }

    // Call the detector
    _detectionInProgress = true;
    log((bigDetector?.detectFace(image)).toString());
    // await _arucoDetector.detect(image, _camFrameRotation);
    _detectionInProgress = false;
    _lastRun = DateTime.now().millisecondsSinceEpoch;
  }

  @override
  Widget build(BuildContext context) {
    if (_camController == null) {
      return const Scaffold(
        body: Center(
          child: Text('Loading...'),
        ),
      );
    }

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () async {},
      ),
      backgroundColor: Colors.purple,
      body: Stack(
        children: [
          Positioned(
            bottom: 0,
            top: 0,
            child: CameraPreview(
              _camController!,
            ),
          ),
        ],
      ),
    );
  }
}
